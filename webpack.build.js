var path = require('path');
var glob = require("glob");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports =
{
	target: "node",
	devtool: "source-map",
	output:
	{
		path: "build",
		filename: "[name].js",
		sourceMapFilename: "[name].map",
		chunkFilename: "[id].chunk.js"
	},
	entry:
	{
		"linux-poe-tools": "./src/index.ts"
	},
	resolve:
	{
		extensions: ["", ".ts", ".js", ".json"],
		root: path.resolve("./src"),
		modulesDirectories: ["node_modules"],
	},
	module:
	{
		loaders:
		[
			{ test: /\.ts$/, loaders: ["ts"] },
			{ test: /\.json$/, loader: "json-loader" }
		]
	},
	plugins:
	[
		new CopyWebpackPlugin([{ from: "./src/default-config.json" }], {}),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.CommonsChunkPlugin({ name: ["linux-poe-tools"].reverse() }),
		new webpack.BannerPlugin("#!/usr/bin/env node", {raw: true})
	]
};