import { ItemParser } from './item-parser';
import { Item } from './item';
import http = require("http");

var poeNinjaBaseApiUrl = "http://poe.ninja/api/Data/";
var poeNinjaLeagueQuery = "?league={league}";

export class NinjaApi {

	constructor(private league: string) { }

	getPoeNinjaResourceName(item: Item): string {
		if (item.attacksPerSecond > 0) return "GetUniqueWeaponOverview";
		else if (/flask$/mig.test(item.baseType)) return "GetUniqueFlaskOverview";
		else if (/ring$/mig.test(item.baseType)) return "GetUniqueAccessoryOverview";
		else if (/amulet$/mig.test(item.baseType)) return "GetUniqueAccessoryOverview";
		else if (/belt$/mig.test(item.baseType)) return "GetUniqueAccessoryOverview";
		else if (/jewel$/mig.test(item.baseType)) return "GetUniqueJewelOverview";
		else if (/essence/mig.test(item.name)) return "GetEssenceOverview";
		else if (/card$/mig.test(item.rarity)) return "GetDivinationCardsOverview";
		else if (/fragment/mig.test(item.name) || /breachstone/mig.test(item.name)) return "GetFragmentOverview";
		else if (/map$/mig.test(item.name)) return "GetMapOverview";
		return "GetUniqueArmourOverview";
	};

	price(itemText: string): Promise<string> {
		return new Promise((resolve, reject) =>	{
			var item = new ItemParser().parseItem(itemText);
			var resourceName = this.getPoeNinjaResourceName(item);
			var url = poeNinjaBaseApiUrl + resourceName + poeNinjaLeagueQuery.replace("{league}", this.league);

			http.get(url, (res) => {
				var rawData = "";
				res.on("data", function (chunk) { rawData += chunk });
				res.on("end", function() {
					var lines = JSON.parse(rawData).lines;
					let nameMatches = [];

					for (var i=0; i<lines.length; i++) {
						var itemName = lines[i].name || lines[i].currencyTypeName;
						var chaosValue = lines[i].chaosValue || lines[i].chaosEquivalent;
						var linkCount = lines[i].links;

						if (itemName === item.name) {
							nameMatches.push({ item, linkCount, chaosValue });
						}
					}

					if (!nameMatches.length) {
						resolve(item.name + " not found.");
					}
					else {
						let exactLinkCountMatch = nameMatches.filter(m => m.linkCount == item.maxLinkCount);
						let matchingItem = exactLinkCountMatch.length ? exactLinkCountMatch[0] : null;
						if (!exactLinkCountMatch.length) matchingItem = nameMatches.filter(m => m.linkCount < item.maxLinkCount)[0];
						resolve(matchingItem.item.name + " (" + matchingItem.linkCount + ") - " + matchingItem.chaosValue + "c");
					}
				});
			});
		});
	}
}
