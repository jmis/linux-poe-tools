import { ModifierRange } from './modifier-range';
import { Item } from "./item";

export class ItemParser {
	private readonly apsRegex: RegExp = /Attacks per Second: ([^\s]+).*$/mig;
	private readonly physicalRegex = /Physical Damage: (\d+)-(\d+)/ig;
	private readonly fireRegex = /Adds (\d+) to (\d+) Fire Damage(?!.*spells)/ig;
	private readonly coldRegex = /Adds (\d+) to (\d+) Cold Damage(?!.*spells)/ig;
	private readonly lightningRegex = /Adds (\d+) to (\d+) Lightning Damage(?!.*spells)/ig;
	private readonly chaosRegex = /Adds (\d+) to (\d+) Chaos Damage(?!.*spells)/ig;
	private readonly rarityRegex = /Rarity: (.*)$/mig;
	private readonly socketRegex = /Sockets: (.*)$/mig;

	damagePerSecond(itemText: string): string {
		var item = this.parseItem(itemText);
		var output = "";
		output += "Total:     " + item.total.dps(item.attacksPerSecond) + "\n";
		output += "Physical:  " + item.physical.dps(item.attacksPerSecond) + "\n";
		output += "Fire:      " + item.fire.dps(item.attacksPerSecond) + "\n";
		output += "Cold:      " + item.cold.dps(item.attacksPerSecond) + "\n";
		output += "Lightning: " + item.lightning.dps(item.attacksPerSecond) + "\n";
		output += "Chaos:     " + item.chaos.dps(item.attacksPerSecond) + "\n";
		output += "Elemental: " + item.elemental.dps(item.attacksPerSecond);
		return output;
	}

	parseIntRange(rangeRegex: RegExp, text: string): ModifierRange {
		var matches = rangeRegex.exec(text);
		if (!matches) return new ModifierRange(0, 0);
		var min = parseInt(matches[1]) || 0;
		var max = parseInt(matches[2]) || 0;
		return new ModifierRange(min, max);
	}

	createRangeFromText(regex: RegExp, itemText: string) : ModifierRange {
		var matches = regex.exec(itemText);
		if (!matches) return new ModifierRange(0, 0);
		return new ModifierRange(parseInt(matches[1]), parseInt(matches[2]));
	}

	parseAttacksPerSecond(itemText: string): number {
		var parsedAps = this.apsRegex.exec(itemText);
		return parsedAps ? parseFloat(parsedAps[1]) : 0;
	}

	parseMaxLinkCount(itemText: string): number {
		let matches = this.socketRegex.exec(itemText);
		if (!matches) return 0;
		let socketsText = matches[1];
		let linkGroups = socketsText.split(" ");
		let linkGroupCounts = linkGroups.map(g => g.split("-").length);
		return Math.max.apply(null, linkGroupCounts);
	}

	parseItem(itemText: string): Item {
		var item = new Item();
		item.rarity = this.rarityRegex.exec(itemText)[1];
		item.name = itemText.split("\n")[1];
		item.baseType = itemText.split("\n")[2];
		item.attacksPerSecond = this.parseAttacksPerSecond(itemText);
		item.physical = this.createRangeFromText(this.physicalRegex, itemText);
		item.fire = this.createRangeFromText(this.fireRegex, itemText);
		item.cold = this.createRangeFromText(this.coldRegex, itemText);
		item.lightning = this.createRangeFromText(this.lightningRegex, itemText);
		item.chaos = this.createRangeFromText(this.chaosRegex, itemText);
		item.elemental = item.fire.add(item.cold).add(item.lightning);
		item.total = item.elemental.add(item.physical).add(item.chaos);
		item.maxLinkCount = this.parseMaxLinkCount(itemText);
		return item;
	};
}
