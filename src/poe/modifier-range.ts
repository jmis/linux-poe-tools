export class ModifierRange {
	constructor(private min: number, private max: number) { }

	dps(aps: number) {
		return (((this.min + this.max) / 2) * aps).toFixed();
	}

	add(otherRange: ModifierRange) {
		return new ModifierRange(this.min + otherRange.min, this.max + otherRange.max);
	}
}