import { ModifierRange } from './modifier-range';

export class Item {
	public physical: ModifierRange;
	public cold: ModifierRange;
	public fire: ModifierRange;
	public lightning: ModifierRange;
	public chaos: ModifierRange;
	public elemental: ModifierRange;
	public total: ModifierRange;
	public rarity: string;
	public name: string;
	public attacksPerSecond: number;
	public baseType: string;
	public maxLinkCount: number;
}