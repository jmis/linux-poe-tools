import { XClient } from './client';
import { Observable, Subject } from "rxjs/Rx";
var x11 = require("x11");
var keysym = require("../keysym");
import "keysym/data/keysyms.json"


export class XWindow {
	private keyListener: (key: string) => void;
	public pingSubject: Subject<boolean>;

	constructor(private window: number, private display: any, private Test: any, private client: XClient) {
		display.client.ChangeWindowAttributes(window, { eventMask: x11.eventMask.KeyPress });
		display.client.on("event", (ev: any) => { if (ev.type == 2) this.keyPressed(ev); });
		this.pingSubject = new Subject<boolean>();
		this.startPinging();
	}

	addKeyListener(f: (key: string) => void) {
		this.keyListener = f;
	}

	keyPressed(e: any) {
		var ks = this.client.getKeySymfromXKeyCode(e.keycode);
		var keyName = keysym.fromKeysym(ks).names[0];
		this.keyListener(keyName);
	}

	type(text: string) {
		for (var i=0; i<text.length; i++) {
			var unicode = text.charCodeAt(i);
			var ks = keysym.fromUnicode(unicode)[0].keysym
			var xKeyCode = this.client.getXKeyCodeFromKeySym(ks);

			this.Test.FakeInput(this.Test.KeyPress, xKeyCode, 0, this.window, 0, 0);
			this.Test.FakeInput(this.Test.KeyRelease, xKeyCode, 0, this.window, 0, 0);
		}
	}

	pings(): Observable<boolean> {
		return this.pingSubject;
	}

	private startPinging(): void {
		let interval = setInterval(() => {
			this.display.client.QueryTree(this.window, (err: any, tree: any) => {
				this.pingSubject.next(err === null);
				if (err !== null) {
					this.pingSubject.complete();
					clearInterval(interval);
					return true;
				}
			});
		}, 1000);
	}
}
