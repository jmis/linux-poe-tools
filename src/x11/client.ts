var x11 = require("x11");

import { XWindow } from "./window";
import child_process = require("child_process");

export class XClient {
	private client: any;
	private window: XWindow;

	constructor(private display: any, private keyboardMapping: any, private keycodeOffset: any) {
		this.client = display.client;
		this.window = null;
	}

	static create(): Promise<XClient> {
		return new Promise(resolve => {
			x11.createClient((err: any, display: any) => {
				display.client.GetKeyboardMapping(display.min_keycode, display.max_keycode - display.min_keycode, (err: any, list: any) => {
					resolve(new XClient(display, list, display.min_keycode));
				});
			});
		});
	}

	isAttached(): boolean {
		return this.window !== null;
	}

	attachToWindow(windowName: string): Promise<XWindow> {
		return new Promise(resolve => {
			this.client.require("xtest", (err: any, Test: any) => {
				this.findWindow(windowName).then((windowId: number) => {
					this.window = new XWindow(windowId, this.display, Test, this);
					this.window.pings().last().subscribe(() => this.window = null);
					resolve(this.window);
				});
			});
		});
	}

	getClipboardText(): string {
		return child_process.spawnSync("xclip", ["-selection", "clip-board", "-o"]).stdout.toString();
	}

	displayText(text: string, displayTimeInMilliseconds: number) {
		var lines = text.split("\n");
		var maxLength = lines[0].length;

		for (var i=0; i<lines.length; i++)
			if (lines[i].length > maxLength)
				maxLength = lines[i].length;

		var white = this.display.screen[0].white_pixel;
		var black = this.display.screen[0].black_pixel;
		var popupWindow = this.client.AllocID();
		this.client.CreateWindow(popupWindow, this.display.screen[0].root, 500, 500, 10 + maxLength * 6, 10 + lines.length * 15, 0, 0, 0, 0, { backgroundPixel: white, overrideRedirect: true });

		var gc = this.client.AllocID();
		this.client.CreateGC(gc, popupWindow, { foreground: black, background: white } );
		this.client.MapWindow(popupWindow);
		setTimeout(() => { this.client.DestroyWindow(popupWindow); }, displayTimeInMilliseconds);

		for (var i=0; i<lines.length; i++) {
			this.client.PolyText8(popupWindow, gc, 5, 15 + (i * 15), lines[i]);
		}
	}

	getXKeyCodeFromKeySym(ks: any) {
		for (var j=0; j<this.keyboardMapping.length; j++)
			for (var k=0; k<this.keyboardMapping[j].length; k++)
				if (this.keyboardMapping[j][k] == ks)
					return j + this.keycodeOffset;

		return null;
	}

	getKeySymfromXKeyCode(keyCode: number): number {
		return this.keyboardMapping[keyCode - this.keycodeOffset][0];
	}

	getAllWindows(window: number): Promise<Array<number>> {
		return new Promise(resolve => {
			this.getChildren(window).then(children => {
				var childPromises = new Array<Promise<Array<number>>>();

				for (var i=0; i<children.length; i++) {
					childPromises.push(this.getAllWindows(children[i]));
				}

				Promise.all(childPromises).then(results => {
					var allChildren = results.reduce(function (x, y) { return x.concat(y); }, []);
					resolve([window].concat(allChildren));
				});
			});
		});
	}

	getChildren(window: number): Promise<Array<number>> {
		return new Promise(resolve => {
			this.display.client.QueryTree(window, (err: any, tree: any) => {
				resolve(tree.children);
			});
		});
	}

	getWindowName(window: number): Promise<any> {
		return new Promise(resolve => {
			this.display.client.GetProperty(0, window, this.display.client.atoms.WM_NAME, this.display.client.atoms.STRING, 0, 10000000, (err: any, prop: any) => {
				resolve({ window: window, prop: prop });
			});
		});
	}

	findWindow(windowName: string): Promise<number> {
		return new Promise(resolve => {
			this.getAllWindows(this.display.screen[0].root).then(allWindows => {
				Promise.all(allWindows.map(window => { return this.getWindowName(window); })).then(windowNames => {
					for (var i=0; i<windowNames.length; i++)
						if (windowNames[i].prop.data.toString() == windowName)
							resolve(windowNames[i].window);
				});
			});
		});
	}
}

