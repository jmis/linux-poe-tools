import { Subject, Observable } from 'rxjs/Rx';
var spawn = require("child_process").spawn;
var os = require("os");

export class MemoryUsage {
	total: number;
	used: number;
	free: number;
	shared: number;
	buffers: number;
	cached: number;
	actualFree: number;
	percentUsed: number;
	comparePercentUsed: string;
}

export class MemoryMonitor {
	private subject: Subject<MemoryUsage>;

	constructor() {
		this.subject = new Subject<MemoryUsage>();
	}

	public startWatching(): Observable<MemoryUsage> {
		let prc = spawn("free", []);
		prc.stdout.setEncoding("utf8");
		prc.stdout.on("data", (data: string) => {
			let lines = data.toString().split(/\n/g),
				line = lines[1].split(/\s+/),
				total = parseInt(line[1], 10),
				free = parseInt(line[3], 10),
				buffers = parseInt(line[5], 10),
				cached = parseInt(line[6], 10),
				actualFree = free + buffers + cached,
				used = parseInt(line[2], 10);

			this.subject.next({
				total: total,
				used: used,
				free: free,
				shared: parseInt(line[4], 10),
				buffers: buffers,
				cached: cached,
				actualFree: actualFree,
				percentUsed: used / total * 100,
				comparePercentUsed: ((1 - (os.freemem() / os.totalmem())) * 100).toFixed(2)
			});
		});

		return this.subject;
	}
}
