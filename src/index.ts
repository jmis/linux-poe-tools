/// <reference path="../node_modules/@types/node/index.d.ts"/>
/// <reference path="../node_modules/@types/es6-promise/index.d.ts"/>

import { MemoryMonitor } from './system/memory';
import { XWindow } from './x11/window';
import { NinjaApi } from './poe/ninja';
import { XClient } from './x11/client';
import { ItemParser } from './poe/item-parser';
import fs = require("fs");
import path = require("path");

var homeDirectory = process.env["HOME"];
var configLocation = homeDirectory + "/.config/linux-poe-tools.json";

if (!fs.existsSync(configLocation))
{
	var defaultConfigDirectory = path.dirname(process.argv[1]);
	var defaultConfig = fs.readFileSync(defaultConfigDirectory + "/default-config.json", "utf8");
	fs.writeFileSync(configLocation, defaultConfig);
}

var configuration = JSON.parse(fs.readFileSync(configLocation, "utf8"));
var defaultTextDisplayTime = parseInt(configuration.popup.displayTime);
var actions = configuration.actions;

if (!configuration.league) configuration.league = "Harbinger";

console.log("League: " + JSON.stringify(configuration));

function attachWindow(client: XClient): void {
	client.attachToWindow("Path of Exile").then(window => {
		window.addKeyListener(keyName => {
			try {
				var action = actions[keyName];
				if (action == "remaining") window.type("\r/remaining\r");
				else if (action == "hideout") window.type("\r/hideout\r");
				else if (action == "dps") client.displayText(new ItemParser().damagePerSecond(client.getClipboardText()), defaultTextDisplayTime);
				else if (action == "price") new NinjaApi(configuration.league).price(client.getClipboardText()).then(function (output) { client.displayText(output, defaultTextDisplayTime); });
			}
			catch (e) {
				client.displayText(e.message + "\n" + e.stack, defaultTextDisplayTime);
			}
		});
	});
}

let warningThresholds = [50, 60, 70, 80, 90];
let lastThresholdIndex = 0;

XClient.create().then(client => {
	setInterval(() => {
		if (!client.isAttached()) {
			attachWindow(client);
			lastThresholdIndex = 0;
		}
	}, 1000);

	setInterval(() => {
		new MemoryMonitor().startWatching().subscribe(memory => {
			if (memory.percentUsed >= warningThresholds[lastThresholdIndex]) {
				client.displayText("Memory warning: " + memory.percentUsed.toFixed(2) + "\nPassed threshold index: " + lastThresholdIndex, 5000);
				lastThresholdIndex++;
			}
		});
	}, 1000);
});
