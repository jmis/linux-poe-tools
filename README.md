***linux-poe-tools***

Similar to the AHK tools available on Windows, linux-poe-tools provides macros and item information to Wine users.  These tools work by listening to X server key press events and displaying popups over the game.  Contact GGG if you have ToS violation concerns.

The following operations are supported:
* (F2) Show remaining
* (F3) Goto hideout
* (F4) Show weapon DPS
* (F6) Lookup price on poe.ninja

Hotkeys can be changed by editing the ~/.config/linux-poe-tools.json file.  If you run into issue, try deleting the config file.

Additional Features:
* Memory monitor with notifications
* Auto window attachment - Launch once and linux-poe-tools will re-attach if the game restarts.

*Setup*

~~~~
sudo apt-get install nodejs npm xclip
~~~~

*Building*

~~~~
npm install
npm run build
npm start
~~~~

*Installation*

~~~~
sudo npm install -g linux-poe-tools
~~~~

*Running*

~~~~
linux-poe-tools
~~~~

